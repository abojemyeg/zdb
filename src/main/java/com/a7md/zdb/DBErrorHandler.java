package com.a7md.zdb;

public interface DBErrorHandler {
    void handle_error(Throwable error);
}
