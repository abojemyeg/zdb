package com.a7md.zdb;

public interface Writer<C, V> {
    void set(C c, V v);
}
